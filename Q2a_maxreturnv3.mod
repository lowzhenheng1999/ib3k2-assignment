param n; #number of nodes
set node_labels:= 1..n;

param a; #number of assets
set A:= 1..a;

param c:= 0.02; #transaction cost

param bench {i in A}; #benchmark portfolio, 0.15 everything

param initial {i in A}; #initial positions, 0.2 everything

param branch_prob {i in node_labels}; #branch probability

param r {i in node_labels, j in A}; #return prob indexed [node label, asset no.]

param vcmatrix {i in A, j in A}; #variance-covariance matrix

param benchmark {i in node_labels, j in A}=
	(if (1<= i<= 4) then (bench[j]) else(	
		if (5<= i<= 8) then (benchmark[1,j]) else(
			if (9<= i<= 12) then (benchmark[2,j]) else(
				if (13<= i<= 16) then (benchmark[3,j]) else(
					benchmark[4,j])))))*(1 + r[i,j]); 
#calculates the return of each asset in the benchmark portfolio
					
param benchmark_w {i in node_labels, j in A}=  
	benchmark[i,j]/(benchmark[i,1] + benchmark[i,2] + benchmark[i,3] + benchmark[i,4] + benchmark[i,5]); #
#calculates the weight of each asset relative to the return at that time period, ie sum to 1


#Variables
var asset_allocation {i in A} >= 0; #initial asset allocation after restructuring from current position

var initial_buy {i in A}>= 0; 
var initial_sell {i in A}>= 0;

var asset {i in node_labels, j in A}>= 0; #return of assets at t= 1, 2
var asset_w{i in node_labels, j in A}>= 0; #weight of assets at t = 1, 2

var buy {i in node_labels, j in A}>= 0; 
var sell {i in node_labels, j in A}>= 0;

#Objective

maximize wealth:
	branch_prob[1]*branch_prob[5]*(sum{j in A} ((asset[1,j] - asset_allocation[j]) - (benchmark[1,j] - bench[j]))) +
	branch_prob[1]*branch_prob[6]*(sum{j in A} ((asset[1,j] - asset_allocation[j]) - (benchmark[1,j] - bench[j]))) +
	branch_prob[1]*branch_prob[7]*(sum{j in A} ((asset[1,j] - asset_allocation[j]) - (benchmark[1,j] - bench[j]))) +
	branch_prob[1]*branch_prob[8]*(sum{j in A} ((asset[1,j] - asset_allocation[j]) - (benchmark[1,j] - bench[j]))) +
	branch_prob[2]*branch_prob[9]*(sum{j in A} ((asset[2,j] - asset_allocation[j]) - (benchmark[2,j] - bench[j]))) +
	branch_prob[2]*branch_prob[10]*(sum{j in A} ((asset[2,j] - asset_allocation[j]) - (benchmark[2,j] - bench[j]))) +
	branch_prob[2]*branch_prob[11]*(sum{j in A} ((asset[2,j] - asset_allocation[j]) - (benchmark[2,j] - bench[j]))) +
	branch_prob[2]*branch_prob[12]*(sum{j in A} ((asset[2,j] - asset_allocation[j]) - (benchmark[2,j] - bench[j]))) +
	branch_prob[3]*branch_prob[13]*(sum{j in A} ((asset[3,j] - asset_allocation[j]) - (benchmark[3,j] - bench[j]))) +
	branch_prob[3]*branch_prob[14]*(sum{j in A} ((asset[3,j] - asset_allocation[j]) - (benchmark[3,j] - bench[j]))) +
	branch_prob[3]*branch_prob[15]*(sum{j in A} ((asset[3,j] - asset_allocation[j]) - (benchmark[3,j] - bench[j]))) +
	branch_prob[3]*branch_prob[16]*(sum{j in A} ((asset[3,j] - asset_allocation[j]) - (benchmark[3,j] - bench[j]))) +
	branch_prob[4]*branch_prob[17]*(sum{j in A} ((asset[4,j] - asset_allocation[j]) - (benchmark[4,j] - bench[j]))) +
	branch_prob[4]*branch_prob[18]*(sum{j in A} ((asset[4,j] - asset_allocation[j]) - (benchmark[4,j] - bench[j]))) +
	branch_prob[4]*branch_prob[19]*(sum{j in A} ((asset[4,j] - asset_allocation[j]) - (benchmark[4,j] - bench[j]))) +
	branch_prob[4]*branch_prob[20]*(sum{j in A} ((asset[4,j] - asset_allocation[j]) - (benchmark[4,j] - bench[j])));
	
#since we are already multipling with (1 + r) in the constraints, asset - asset_allcoatio 
#will give us the return on the assets, then we minus (benchmark - bench), which gives the
#return relative to the benchmark 

#Constraints
subject to asset_t0 {i in A}:
	initial[i] + (1 - c)*initial_buy[i] - (1 + c)*initial_sell[i] = asset_allocation[i]; 
#restructuring portfolio from initial investment

subject to transaction_t0:
	sum{i in A} (initial_buy[i] - initial_sell[i]) = 1 - sum{i in A} initial[i];
#to ensure that we dont sell everything and are "restructuring the portfolio"
	
subject to asset_t1 {i in node_labels, j in A: 1<= i<= 4}:
	(asset_allocation[j])*(1 + r[i,j]) + (1 - c)*buy[i,j] - (1 + c)*sell[i,j] = asset[i,j];
#return of assets in t1 after multiplying weights by (1 + rate of return)

subject to transaction_t1 {i in node_labels: 1<= i<= 4}: 
	sum{j in A}(buy[i,j] - sell[i,j]) = 0;
#to ensure that we dont sell everything and are "restructuring the portfolio"
		
subject to asset_t2 {i in node_labels, j in A: 5<= i<= 20}:
	(if (5<= i<= 8) then (asset[1,j]) else(
		if (9<= i<= 12) then (asset[2,j]) else(
			if (13<= i<= 16) then (asset[3,j]) else(
				asset[4,j]))))*(1 + r[i,j]) = asset[i,j]; 
#return of assets at the last time step, no buying and selling in last period

#subject to asset_weights {i in node_labels, j in A}:
	#asset[i,j]/(asset[i,1] + asset[i,2] + asset[i,3] + asset[i,4] + asset[i,5]) = asset_w[i,j];

									 

	
	
	
	
	
	
